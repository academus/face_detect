from flask import Flask
import os
from .face_get_size import detect_face_gray, get_video, get_face_ok


def create_app():
    app = Flask(__name__)
    from .face_web_interface import bp_front_app
    app.register_blueprint(bp_front_app)
    #app.debug
    return app