import numpy as np
import cv2
import face_recognition
import base64
from flask_opencv_streamer.streamer import Streamer

port = 3030
require_login = False
streamer = Streamer(port, require_login)

#Minimun size to Detect
size_face_w=250
size_face_h=250

face_cascade = cv2.CascadeClassifier('venv/lib/python3.7/site-packages/cv2/data/haarcascade_frontalface_alt2.xml')

def get_face_ok(w, h):
    if (w >= size_face_w) and (h >= size_face_h):
        
        color = (0, 255, 0) #BGR
        return color, 'Face Detected', True
    else:
        color = (0, 0, 255) #BGR 
        return color, 'Detecting', False

def get_video():
    #Open Webcam Caputre
    cap = cv2.VideoCapture(0)
    #Get frame by frame
    ret, frame = cap.read()
    #convert image to gray
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    cap.release()
    return ret, frame, gray

def detect_face_gray():
    ret, frame, gray = get_video() 
    encoded_string = ''
    status = ''
    faces = face_cascade.detectMultiScale(gray, scaleFactor=1.2, minNeighbors=3, minSize=(170, 170))
    for (x, y, w, h) in faces:

        #convert first to jpg
        #this is necessary to treat future actions
        (flag, encodedImage) = cv2.imencode(".jpg", frame)
        #encode to base64
        encoded_string = base64.b64encode(encodedImage)



        color, name, status = get_face_ok(w, h) #BGR
        stroke = 2
        #Set coordinates of end image region
        end_cord_x = x + w
        end_cord_y = y + h
        #Draw rectagle on face
        cv2.rectangle(frame, (x, y), (end_cord_x, end_cord_y), color, stroke)
        #Define Font text to image
        font = cv2.FONT_HERSHEY_DUPLEX
        #Draw text on below face
        cv2.putText(frame, name, (x , end_cord_y + 20), font, 1.0, (255, 255, 255), 1)
        return frame, encoded_string, status
    return frame, encoded_string, status

def detect_face_color():
    ret, frame, gray = get_video()
    rgb_frame = frame[:, :, ::-1]
    face_locations = face_recognition.face_locations(rgb_frame)
    
    for (x, y, w, h) in face_locations:       
        
        stroke = 2
        width = w - x
        height = h + y
        color, name = get_face_ok(width, height) #BGR
        # Draw a box around the face
        cv2.rectangle(frame, (h, x), (y, w), color, stroke)
        #cv2.rectangle(frame, (x, y), (end_cord_x, end_cord_y), color, stroke)

        # Draw a label with a name below the face
        #cv2.rectangle(frame, (h, x), (y, w), color, stroke)
        font = cv2.FONT_HERSHEY_DUPLEX
        cv2.putText(frame, name, (x + 50 , w + 20), font, 1.0, (255, 255, 255), 1)
        return frame
    return frame

#while(True):
    # Display de result
#    frame, encoded_string = detect_face_gray()
    #print(encoded_string)
#    cv2.imshow('frame',frame)
#    if cv2.waitKey(20) & 0xFF == ord('q'):
    #    print (encoded_string)
#        break

#cv2.destroyAllWindows()
