 # Configuração Ambiente
 
 Iniciandio Ambiente Virtual:
   - python3.7 -m venv venv
   - source venv/bin/activate

Instalando dependências:
  - Flask
  - opencv-python
  - wheel
  - blueprint
  - numpy
  - face_recognition
  - opencv-contrib-python==4.1.0.25

  
Importar Variaveis:
export FLASK_DEBUG=True
export FLASK_ENV=Development
export FLASK_APP=./